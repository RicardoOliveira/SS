package br.com.senac.livro;

import java.util.List;
import javax.swing.DefaultListModel;


public class Livro {
    
    private String titulo;
    private String isbn;
    private String anoEdicao;
    private String editora;
    private Genero genero;
    private  DefaultListModel<Autor> autor = new DefaultListModel<>();
    
    




    
    


    //Construtores;
    /***
     * Existem 3 construtores 
     *          1º com todos como argumento;
     *          2º com apenas 4 como argumentos, sem o gênero;
     *          3º sem nada;
     */
    public Livro(String titulo, String isbn, String anoEdicao, String editora, Genero genero,DefaultListModel<Autor> autor) {
        this.titulo = titulo;
        this.isbn = isbn;
        this.anoEdicao = anoEdicao;
        this.editora = editora;
        this.genero = genero;
        this.autor = autor;
    }
    public Livro() {
    }
    public Livro(String titulo, String isbn, String anoEdicao, String editora) {
        this.titulo = titulo;
        this.isbn = isbn;
        this.anoEdicao = anoEdicao;
        this.editora = editora;
    }
    
    
    
    
    
    
    
    
    
    
    
    

    @Override
    public String toString() {
        return " Autores:"+ autor +" "+  this.titulo +" -##- "  +" -##- "+ this.genero +" -##- "+ this.editora +" -##- "+this.anoEdicao+" -##- "+this.isbn;
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public DefaultListModel<Autor> getAutor() {
        return this.autor;
    }

    public void setAutor(DefaultListModel<Autor> autor) {
        this.autor = autor;
    }

    
    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getAnoEdicao() {
        return anoEdicao;
    }
    public void setAnoEdicao(String anoEdicao) {
        this.anoEdicao = anoEdicao;
    }

    public String getEditora() {
        return editora;
    }
    public void setEditora(String editora) {
        this.editora = editora;
    }

    public Genero getGenero() {
        return genero;
    }
    public void setGenero(Genero genero) {
        this.genero = genero;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
