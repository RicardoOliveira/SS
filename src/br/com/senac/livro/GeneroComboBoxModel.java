package br.com.senac.livro;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import javax.swing.event.ListDataListener;

public class GeneroComboBoxModel  extends AbstractListModel implements ComboBoxModel{
    private List<Genero> listaGenero;
    private Genero obtendoGenero;
    
    public GeneroComboBoxModel(){
    listaGenero = new ArrayList<>();    
    }
    
    
    @Override
    public void setSelectedItem(Object o) {
        if(o instanceof Genero){
            this.obtendoGenero = (Genero)o;
             fireContentsChanged(this.listaGenero, 0, this.listaGenero.size());
        }
    }

    @Override
    public Object getSelectedItem() {
        return this.obtendoGenero;
    }

    @Override
    public int getSize() {
        return listaGenero.size();
    }

    @Override
    public Object getElementAt(int i) {
      return  this.listaGenero.get(i);
    }
    
    public void addGenero(Genero genero){
        this.listaGenero.add(genero);
    }

    public void resetar(){
        this.listaGenero.clear();
    }
    
    
    @Override
    public void addListDataListener(ListDataListener ll) {
        
    }

    
    
    @Override
    public void removeListDataListener(ListDataListener ll) {
    }
    
    
    
    
    
    
}
