package br.com.senac.livro;
public class Autor {
    
    
    private String nomeAutor;

    
    @Override
    public String toString(){
        return this.nomeAutor;
    }
    
    public Autor(String nomeAutor) {
        this.nomeAutor = nomeAutor;
    }

    public Autor() {
    }

    
    
    
    public String getNomeAutor() {
        return nomeAutor;
    }
    public void setNomeAutor(String nomeAutor) {
        this.nomeAutor = nomeAutor;
    }
    
    
    
    
    
}
