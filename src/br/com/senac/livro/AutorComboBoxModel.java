/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livro;

import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

/**
 *
 * @author Ricardo Coimbra
 */
public class AutorComboBoxModel extends AbstractListModel implements ComboBoxModel{
    private  List<Autor>listaAutor;
    private  Autor autorSelecionado;
    
    public AutorComboBoxModel(){
        this.listaAutor = new ArrayList<>();
    }
    
    
    @Override
    public int getSize() {
        return this.listaAutor.size();
    }

    @Override
    public Object getElementAt(int i) {
        return this.listaAutor.get(i);
    }

    @Override
    public void setSelectedItem(Object o) {
        if(o instanceof Autor){
            
    this.autorSelecionado = (Autor) o;
        fireContentsChanged(this.listaAutor, 0, this.listaAutor.size());
    }
        }
public List<Autor> getAutore(){
    return this.listaAutor;
}
    
    @Override
    public Object getSelectedItem() {
    return this.autorSelecionado;
    }
    
    public void addAutores(Autor autor){
        this.listaAutor.add(autor);
            
    }
    
    public void reset(){
        this.listaAutor.clear();
    }
    
    public void removerAutor(Autor autor){
        this.listaAutor.remove(autor);
        fireContentsChanged(this.listaAutor, 0, this.listaAutor.size());
    }
    
    
    
}
