package br.com.senac.livro;
public class Genero {
    
    
    private String descricao;

    
    
    
    public Genero(String descricao) {
        this.descricao = descricao;
    }
    public Genero() {
    }

    @Override
    public String toString() {
        return this.descricao;
    }

    
    public String getDescricao() {
        return descricao;
    }
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
