package br.com.jframeprincipal.JFrameCadastro;

import br.com.jframeprincipal.jframepesquisa.JFramePesquisarLivro;
import br.com.senac.livro.Autor;
import br.com.senac.livro.Genero;
import br.com.senac.livro.Livro;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import sysbook.SysBook;

/**
 *
 * @author ricardo.coimbra
 */
public class JFrameCadastrarLivro extends javax.swing.JFrame {

    /**
     * Duas variáveis declaradas para preencher o jList no quinto bloco abaixo;
     */
    private List<Autor> selecaoAutores;
    private Autor autorSelecionado;

    /**
     * Construtor JFrame
     */
    public JFrameCadastrarLivro() {
        initComponents();
        selecaoAutores = new ArrayList<>();
        this.setResizable(false);
        adicionandoComboBox();
        preencherListaGenero();

        JFrameCadastrarAutor frameAutor = new JFrameCadastrarAutor();
        frameAutor.passandoParametro(this);

        JFrameCadastrarGenero frameGenero = new JFrameCadastrarGenero();
        frameGenero.passarParametro(this);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtEditoraLivro = new javax.swing.JTextField();
        txtTituloLivro = new javax.swing.JTextField();
        txtIsbnLivro = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBoxGeneroLivro = new javax.swing.JComboBox();
        txtAnoEdicaoLivro = new javax.swing.JFormattedTextField();
        jPanel1 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jComboBoxAutorLivro = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jListAutor = new javax.swing.JList();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Livro");

        jLabel1.setFont(new java.awt.Font("Trajan Pro", 1, 12)); // NOI18N
        jLabel1.setText("Título do Livro");

        txtEditoraLivro.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        txtTituloLivro.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTituloLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTituloLivroActionPerformed(evt);
            }
        });

        txtIsbnLivro.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jLabel2.setFont(new java.awt.Font("Trajan Pro", 1, 12)); // NOI18N
        jLabel2.setText("ISBN");

        jLabel3.setFont(new java.awt.Font("Trajan Pro", 1, 12)); // NOI18N
        jLabel3.setText("Editora");

        jLabel4.setFont(new java.awt.Font("Trajan Pro", 1, 12)); // NOI18N
        jLabel4.setText("Ano de Edição");

        jComboBoxGeneroLivro.setFont(new java.awt.Font("Trajan Pro", 1, 12)); // NOI18N
        jComboBoxGeneroLivro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gênero", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12))); // NOI18N
        jComboBoxGeneroLivro.setPreferredSize(new java.awt.Dimension(37, 39));

        try {
            txtAnoEdicaoLivro.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##/##/####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtAnoEdicaoLivro.setHorizontalAlignment(javax.swing.JTextField.CENTER);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cadastrar?", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trajan Pro", 1, 24))); // NOI18N

        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jframeprincipal/imagens/Cancel-icon.png"))); // NOI18N
        jButton2.setText("Cancelar");
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton2.setIconTextGap(10);
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jframeprincipal/imagens/Programming-Save-As-icon.png"))); // NOI18N
        jButton1.setText("Salvar");
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton1.setIconTextGap(10);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(29, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(jButton2)
                .addGap(28, 28, 28))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(39, Short.MAX_VALUE))
        );

        jComboBoxAutorLivro.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Autor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Trajan Pro", 1, 12))); // NOI18N
        jComboBoxAutorLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBoxAutorLivroActionPerformed(evt);
            }
        });

        jListAutor.setToolTipText("");
        jScrollPane1.setViewportView(jListAutor);

        jButton3.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jframeprincipal/imagens/Document-Add-icon.png"))); // NOI18N
        jButton3.setText("Adicionar Autor");
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton3.setIconTextGap(9);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jframeprincipal/imagens/Document-Delete-icon.png"))); // NOI18N
        jButton4.setText("Deletar da Lista");
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton4.setIconTextGap(9);
        jButton4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButton4MouseClicked(evt);
            }
        });
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setFont(new java.awt.Font("Arial", 1, 12)); // NOI18N
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jframeprincipal/imagens/Delete-icon (1).png"))); // NOI18N
        jButton5.setText("Deletar Autor");
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton5.setIconTextGap(9);
        jButton5.setInheritsPopupMenu(true);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jframeprincipal/imagens/delete-file-icon.png"))); // NOI18N
        jButton6.setText("Deletar Todos");
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton6.setIconTextGap(9);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jframeprincipal/imagens/Delete-icon (1).png"))); // NOI18N
        jButton7.setText("Remover Genero");
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        jButton7.setIconTextGap(9);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(114, 114, 114)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtTituloLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(56, 56, 56)
                                .addComponent(jLabel4))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtIsbnLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel3)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtEditoraLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAnoEdicaoLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton6)
                        .addGap(141, 141, 141))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 74, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jComboBoxAutorLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jComboBoxGeneroLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 259, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton7))
                                .addGap(0, 121, Short.MAX_VALUE))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBoxGeneroLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtTituloLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtAnoEdicaoLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(5, 5, 5)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton7)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtIsbnLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEditoraLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(83, 83, 83)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jComboBoxAutorLivro, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jButton5)))
                        .addGap(6, 6, 6)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton6)
                            .addComponent(jButton4))))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents


    
    
    
    
    
    
    
    
    
    
    
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
        this.setVisible(false);
        listaAutor.removeAllElements();


    }//GEN-LAST:event_jButton2ActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Cadastrando o objeto livro;
     *
     * @param evt
     */
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        DefaultListModel<Autor> listaDeAutor = new DefaultListModel<>();
        String tituloLivro = txtTituloLivro.getText();
        String anoEdicao = txtAnoEdicaoLivro.getText();
        String editora = txtEditoraLivro.getText();
        String isbn = txtIsbnLivro.getText();
        Genero genero = (Genero) jComboBoxGeneroLivro.getSelectedItem();
        listaDeAutor = (DefaultListModel<Autor>) jListAutor.getModel();
        Livro livro = new Livro(tituloLivro, isbn, anoEdicao, editora, genero, listaDeAutor);
        SysBook.addLivro(livro);
        this.limpar();
        JOptionPane.showMessageDialog(this, "Dados salvos com sucesso", "Mensagem Importante", JOptionPane.INFORMATION_MESSAGE);

    }//GEN-LAST:event_jButton1ActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Preenchendo o jList com aquelas duas variáveis declaradas no início da
     * classe; Utilizamos a classe DefaultListModel e tipamos a variável para
     * "Autor"; Porque o jList só seta o Modelo utilizando a clase
     * DefaultListModel;
     */
    DefaultListModel<Autor> listaAutor = new DefaultListModel<>();
    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed

        for (int c = 0; c < listaAutor.size(); c++) {
            if (jComboBoxAutorLivro.getSelectedItem().equals(selecaoAutores.get(c))) {
                JOptionPane.showMessageDialog(this, "Este autor já existe na sua lista. Favor inserir outro.");
                return;
            }
        }

        if (jComboBoxAutorLivro.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "Você deve inserir um autor no campo 'Autor' para depois adicionar a lista.", "Erro", JOptionPane.ERROR_MESSAGE);
        } else if (jComboBoxAutorLivro.getSelectedItem() != null) {
            int resposta = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja inserir o autor na lista? ");
            switch (resposta) {
                case 0:
                    autorSelecionado = (Autor) jComboBoxAutorLivro.getSelectedItem();
                    selecaoAutores.add(autorSelecionado);
                    listaAutor.clear();
                    for (int i = 0; i < this.selecaoAutores.size(); i++) {
                        listaAutor.add(i, this.selecaoAutores.get(i));
                    }
                    this.jListAutor.setModel(listaAutor);
            }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Quando eu clico no botão Deletar. Está sendo deletado cada objeto do meu
     * jList.
     *
     * @param evt
     */
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed

        if (listaAutor.isEmpty()) {
            JOptionPane.showMessageDialog(this, "A lista está vazia para deletar", "Erro", JOptionPane.ERROR_MESSAGE);
        } else if (!listaAutor.isEmpty() && jListAutor.getSelectedValue() == null) {
            JOptionPane.showMessageDialog(this, "Primeiro você deverá selecionar um autor para excluir.", "Erro", JOptionPane.ERROR_MESSAGE);
        } else {
            Autor a = (Autor) jListAutor.getSelectedValue();
            SysBook.removeAutor(a);
            listaAutor.removeElement(a);
            selecaoAutores.remove(a);
            jListAutor.setModel(listaAutor);

        }


    }//GEN-LAST:event_jButton4ActionPerformed


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    private void jButton4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton4MouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_jButton4MouseClicked

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Quando eu clico no botao Deletar Autor. Em continuação
     *
     * @param evt
     */
    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if (jComboBoxAutorLivro.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "Não existe autor para ser excluído.", "Erro", JOptionPane.ERROR_MESSAGE);
        } else {
            Autor a = (Autor) jComboBoxAutorLivro.getSelectedItem();
            SysBook.removeAutor(a);
            modelAutores.removeElement(a);
            jComboBoxAutorLivro.setModel(modelAutores);
        }

    }//GEN-LAST:event_jButton5ActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Quando eu clico no botão "Deletar Todos". Neste código, está sendo feita
     * a exclusão de todos os elementos do jList; Inclusive na listaAutore
     * também na selecaoAutores; utilizadas para preencher o jList;
     *
     * @param evt
     */
    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if (listaAutor.isEmpty()) {
            JOptionPane.showMessageDialog(this, "A lista já se encontra vazia.", "Erro", JOptionPane.ERROR_MESSAGE);
        } else if(!listaAutor.isEmpty()) {
           int resposta = JOptionPane.showConfirmDialog(this, "Tem certeza que deseja excluir toda a lista?", "Confirmação", JOptionPane.INFORMATION_MESSAGE);
           switch(resposta){
               case 0:
        listaAutor.clear();
        listaAutor.removeAllElements();
        selecaoAutores.removeAll(selecaoAutores);
        jListAutor.setModel(listaAutor);
                   break;
               case 1:
                   return;
           }
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        if (jComboBoxGeneroLivro.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(this, "Não existe gênero para ser excluído.", "Erro", JOptionPane.ERROR_MESSAGE);
        } else {
            Genero a = (Genero) jComboBoxGeneroLivro.getSelectedItem();
            SysBook.removeGenero(a);
            modelGenero.removeElement(a);
            jComboBoxGeneroLivro.setModel(modelGenero);
        }


    }//GEN-LAST:event_jButton7ActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    private void txtTituloLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTituloLivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTituloLivroActionPerformed


    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    private void jComboBoxAutorLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBoxAutorLivroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBoxAutorLivroActionPerformed

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Adicionando objetos ao jComboBoxAutor;
     */
    DefaultComboBoxModel<Autor> modelAutores = new DefaultComboBoxModel<>();
    private void adicionandoComboBox() {
        for (Autor autorcadastrado : SysBook.getListaAutor()) {
            modelAutores.addElement(autorcadastrado);
        }
        this.jComboBoxAutorLivro.setModel(modelAutores);
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * Adicionando objetos ao jComboBoxGenero;
     */
    DefaultComboBoxModel<Genero> modelGenero = new DefaultComboBoxModel<>();
    public void preencherListaGenero() {
        for (Genero genero : SysBook.getListGenero()) {
            modelGenero.addElement(genero);
        }
        this.jComboBoxGeneroLivro.setModel(modelGenero);
    }

    
    
    
    
    
    
    
    
    
    
    /**
     * Limpando a tela dos jTextField quando clico em salvar;
     */
    public void limpar() {
        DefaultComboBoxModel<String> limpando = new DefaultComboBoxModel<>();
        limpando.setSelectedItem(null);
        this.txtTituloLivro.setText("");
        this.txtAnoEdicaoLivro.setText("");
        this.txtEditoraLivro.setText("");
        this.txtIsbnLivro.setText("");
        this.jListAutor.setModel(limpando);
       
    }

    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFrameCadastrarLivro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFrameCadastrarLivro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFrameCadastrarLivro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFrameCadastrarLivro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFrameCadastrarLivro().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JComboBox jComboBoxAutorLivro;
    private javax.swing.JComboBox jComboBoxGeneroLivro;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JList jListAutor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JFormattedTextField txtAnoEdicaoLivro;
    private javax.swing.JTextField txtEditoraLivro;
    private javax.swing.JTextField txtIsbnLivro;
    private javax.swing.JTextField txtTituloLivro;
    // End of variables declaration//GEN-END:variables
}
