package sysbook;

import br.com.jframeprincipal.jframeprincipal.JFramePrincipal;
import br.com.senac.cliente.Cliente;
import br.com.senac.livro.Livro;
import br.com.senac.livro.Autor;
import br.com.senac.livro.Genero;
import java.util.ArrayList;
import java.util.List;
import javax.swing.DefaultListModel;

public class SysBook {
    private static List<Autor> listAutor = new ArrayList<>();
    private static List<Cliente> listCliente = new ArrayList<>();
    private static List<Genero> listGenero = new ArrayList<>();
    private static List<Livro> listLivro = new ArrayList<>();

    
    public static void addAutor(Autor autor) {
        listAutor.add(autor);
    }
    public static void removeAutor(Autor autor) {
        listAutor.remove(autor);
    }
    public static int retornarSizeAutor() {
        return listAutor.size();
    }
    public static void addAutorPorIndice(int index, Autor autor) {
        listAutor.add(index, autor);
    }
    public static List<Autor> getListaAutor() {
        return listAutor;
    }
    public static Object getAutorPorIndice(int i){
        return listAutor.indexOf(i);
    }
    
    
    
    public static void addCliente(Cliente cliente) {
        listCliente.add(cliente);
    }
    public static void removeCliente(Cliente cliente) {
        listCliente.remove(cliente);
    }
    public static int retornarSizeCliente() {
        return listCliente.size();
    }
    public static void addClientePorIndice(int index, Cliente cliente) {
        listCliente.add(index, cliente);
    }
    public static List<Cliente> getListaCliente(){
        return listCliente;
    }
    
    
    
    
    public static void addGenero(Genero genero) {
        listGenero.add(genero);
    }
    public static void removeGenero(Genero genero) {
        listGenero.remove(genero);
    }
    public static int retornarSizeGenero() {
        return listGenero.size();
    }
    public static void addGeneroPorIndice(int index, Genero genero) {
        listGenero.add(index, genero);
    }
    public static List<Genero> getListGenero(){
        return listGenero;
    }
    public static Object getGeneroPorIndice(int i){
        return listGenero.indexOf(i);
    }
    
    
    
    
    
    
    public static Livro getLivroPorIndice(int i){
        return listLivro.get(i);
    }
    
    public static void addLivro(Livro livro) {
        listLivro.add(livro);
    }
    public static void removeLivro(Livro livro) {
        listLivro.remove(livro);
    }
    public static int retornarSizeLivro() {
        return listLivro.size();
    }
    public static void addLivroPorIndice(int index, Livro livro) {
        listLivro.add(index, livro);
    }
    
    public static List<Livro> getListLivro(){
        return listLivro;
    }


    


    public static void main(String[] args) {
        new JFramePrincipal().setVisible(true);
        
    }

}
